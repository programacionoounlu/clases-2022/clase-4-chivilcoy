package ar.edu.unlu.utilidades;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class UtilidadesDeFecha {

	public static LocalDate convertirAFecha(String string, FormatosDeFecha formato) throws Exception {
		try {
			return LocalDate.parse(
					string, 
					DateTimeFormatter.ofPattern(
							formato.getMascara()
							)
					);	
		} catch (DateTimeParseException ex) {
			Exception excepcion = new Exception("Fecha no válida");
			throw excepcion;
		}
	}

}
