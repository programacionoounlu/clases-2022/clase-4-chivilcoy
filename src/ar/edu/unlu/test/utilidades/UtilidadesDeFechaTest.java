package ar.edu.unlu.test.utilidades;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Executable;
import java.time.LocalDate;
import java.time.Month;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unlu.utilidades.FormatosDeFecha;
import ar.edu.unlu.utilidades.UtilidadesDeFecha;

class UtilidadesDeFechaTest {
	
	private UtilidadesDeFecha utils;
	
	@BeforeEach
	void before() {
		//this.utils = new UtilidadesDeFecha();
	}
	

	@Test
	void testDadoUnStringConFechaEnFormatoddMMyyyyDeberiaObtenerUnaInstanciaDeLocalDate() throws Exception{
		LocalDate fecha = UtilidadesDeFecha.convertirAFecha(
				"05-02-2001", 
				FormatosDeFecha.DD_MM_YYYY);
		assertEquals(2001, fecha.getYear());
		assertEquals(Month.FEBRUARY, fecha.getMonth());
		assertEquals(5, fecha.getDayOfMonth());
	}
	
	@Test
	void testDadoUnStringConFechaEnFormatoMMddyyyyDeberiaObtenerUnaInstanciaDeLocalDate() throws Exception{		
		LocalDate fecha = UtilidadesDeFecha.convertirAFecha(
				"03-10-1999", 
				FormatosDeFecha.MM_DD_YYYY);
		assertEquals(1999, fecha.getYear());
		assertEquals(Month.MARCH, fecha.getMonth());
		assertEquals(10, fecha.getDayOfMonth());
	}
	
	@Test
	void testDadoUnStringConUnaFechaNoValidaAlConvertirDeberiaGenerarUnaExcepcion(){
		assertThrows(Exception.class, () -> {
			UtilidadesDeFecha.convertirAFecha(
					"03-10-19999", 
					FormatosDeFecha.MM_DD_YYYY);			
		});
	}

}
